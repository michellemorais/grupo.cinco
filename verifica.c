#define lin 50
#define col 203
#define linha 300
#define coluna 3

#define geradorLinha 42
#define geradorColuna 198

typedef struct monstro {
    int posiMonstroX;
    int posiMonstroY;
} MONSTRO;

extern MONSTRO mob;
extern MONSTRO mobs[8];

typedef struct jogador {
    int posx;
    int posy;
} JOGADOR;

extern JOGADOR player;
extern char matriz[lin][col];
extern int x_porta_nivel, y_porta_nivel, score, linha_impressões, qtdMonstros;
extern int matrizaux[linha][coluna], t, p, casa_y, casa_x; // 'casa_x' e 'casa_y' são as coordenadas da posição em estudo da presença ou não de luz


int ehParede(int x, int y){
    return matriz[x][y] == '#';
}

int naoEhParede(int x, int y){
    return !ehParede(x, y);
}

int ehPorta(int x, int y){
    return x == x_porta_nivel && y == y_porta_nivel;
}

int naoEhPorta(int x, int y){
    return !ehPorta(x, y);
}

int ehMonstro(int x, int y){
    for (int i = 0; i < qtdMonstros; ++i) {
        if (x == mobs[i].posiMonstroX && y == mobs[i].posiMonstroY){
            return true;
        }
    }
    return false;
}

int ehMonstroDeprecated(int x, int y){
    return x == mob.posiMonstroX && y == mob.posiMonstroY;
}

int naoEhMonstro(int x, int y){
    return !ehMonstro(x, y);
}

int naoEhPortaOuMonstro(int x, int y){
    return naoEhMonstro(x, y)  && naoEhPorta(x, y);
}

int naoEhParedeOuMonstro(int x, int y){
    return naoEhParede(x, y)  && naoEhMonstro(x, y);
}

int naoEhParedeOuPorta(int x, int y){
    return naoEhParede(x, y) && naoEhPorta(x, y);
}

int ehJogador(int x, int y){
    return x == player.posx && y == player.posy;
}

int naoEhJogador(int x, int y){
    return !ehJogador(x, y);
}

int naoEhParedeOuPortaOuJogador(int x, int y) {
    return naoEhParede(x, y) && naoEhPorta(x, y) && naoEhJogador(x, y);
}
