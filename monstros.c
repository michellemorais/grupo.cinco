void Gera_Nivel(void);

void verifica_vida(void) {
    for (int i=0; i<qtdMonstros; i++)
            if (mobs[i].posiMonstroX == player.posx && mobs[i].posiMonstroY == player.posy) {flash();score=1; Gera_Nivel();}
}

void geraMonstros(int qtd){
    int posxMob_inicial;
    int posyMob_inicial;

    for (int i = 0; i < qtd; ++i) {
        MONSTRO monstro;
        posxMob_inicial = 1 + rand()%geradorLinha;
        do {
            posyMob_inicial = 1 + rand()%geradorColuna;
            if (naoEhParede(posxMob_inicial, posyMob_inicial)) {
                break; //verifica se a posição está livre
            }
        } while (1);

        monstro.posiMonstroX = posxMob_inicial;
        monstro.posiMonstroY = posyMob_inicial;

        mvaddch(monstro.posiMonstroX, monstro.posiMonstroY, 'M' | A_BOLD);

        mobs[i] = monstro;
    }
}

void moveMonstro(MONSTRO *monstro, int moveX, int moveY) {
    if (naoEhParedeOuPorta(monstro->posiMonstroX + moveX, monstro->posiMonstroY + moveY)){
        verifica_vida(); //verifica se o jogador perde, se verdade volta á estaca 0
        attron(COLOR_PAIR(COLOR_YELLOW));
        mvaddch(monstro->posiMonstroX, monstro->posiMonstroY, '.'); //coloca um espaço na posição antiga do jogador /////////////////////////////alterou-se
        attroff(COLOR_PAIR(COLOR_YELLOW));
        monstro->posiMonstroX += moveX;
        monstro->posiMonstroY += moveY;
        attron(COLOR_PAIR(COLOR_CYAN));
        mvaddch(monstro->posiMonstroX, monstro->posiMonstroY, 'M' | A_BOLD); //coloca o jogador na sua nova posição
        attroff(COLOR_PAIR(COLOR_CYAN));
    }
}

void moveMonstros(void){
    for (int i = 0; i < qtdMonstros; ++i) {

        int moveX = 0, moveY = 0;

        if (mobs[i].posiMonstroY < player.posy) {
            moveY = rand()%2;
        }
        if (mobs[i].posiMonstroY > player.posy) {
            moveY = -1 + rand()%2;
        }
        if (mobs[i].posiMonstroX < player.posx) {
            moveX = rand()%2;
        }
        if (mobs[i].posiMonstroX > player.posx) {
            moveX = -1 + rand()%2;
        }
        if (mobs[i].posiMonstroX == player.posx && mobs[i].posiMonstroY < player.posy) {
            moveY = 1;
        }
        if (mobs[i].posiMonstroX == player.posx && mobs[i].posiMonstroY > player.posy) {
            moveY = -1;
        }
        if (mobs[i].posiMonstroX < player.posx && mobs[i].posiMonstroY == player.posy) {
            moveX = 1;
        }
        if (mobs[i].posiMonstroX > player.posx && mobs[i].posiMonstroY == player.posy) {
            moveX = -1;
        }

        moveMonstro(&mobs[i], moveX, moveY);
    }
}

void moveMonstrosDeprecated(void){
    for (int i = 0; i < qtdMonstros; ++i) {
        int moveDirecao = rand()%4;
        switch (moveDirecao) {
            case 0: moveMonstro(&mobs[i], 1, 0); break;
            case 1: moveMonstro(&mobs[i], 0, 1); break;
            case 2: moveMonstro(&mobs[i], -1, 0); break;
            case 3: moveMonstro(&mobs[i], 0, -1); break;
        }
    }
}
