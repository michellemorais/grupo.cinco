#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include <time.h>

#include "iluminacao.c"
#include "jogador.c"

#define lin 50
#define col 203
#define linha 300
#define coluna 3

#define geradorLinha 42
#define geradorColuna 198
MONSTRO mob;
MONSTRO mobs[8];
JOGADOR player;
char matriz[lin][col];
int x_porta_nivel, y_porta_nivel, score=1, linha_impressões, qtdMonstros;
int matrizaux[linha][coluna], t, p, casa_y, casa_x; // 'casa_x' e 'casa_y' são as coordenadas da posição em estudo da presença ou não de luz


int main (void) {


    WINDOW *wnd = initscr();
    int ncols, nrows;
    getmaxyx(wnd,nrows,ncols); //obter o numero de linhas e colunas da janela
    linha_impressões = nrows-1+ncols-ncols;

    start_color(); //inicializa as cores

    cbreak(); //desativa o line buffering, uma interrupt key (interrupt, suspend or quit) é interpretada como um caractere qualquer pelo driver do terminal
    noecho(); //permite fazer echoing de caracteres em qualquer lugar da janela sem atualizar as coordenadas (y,x) atuais
    nonl();
    intrflush(stdscr, false); //não existe flush do buffer se uma interrupt key é pressionada
    keypad(stdscr, true); //permite ao programa receber function keys like F1, F2, arroy keys etc.

    init_pair(COLOR_WHITE, COLOR_WHITE, COLOR_BLACK); //cor branca, background preto
    init_pair(COLOR_YELLOW, COLOR_YELLOW, COLOR_BLACK);
    init_pair(COLOR_MAGENTA, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(COLOR_BLUE, COLOR_BLUE, COLOR_BLACK);
    init_pair(COLOR_CYAN, COLOR_CYAN, COLOR_BLACK);

    Gera_Nivel();

    int i, j;
    while(1) {
        move(lin, 0); //posiciona o cursor no canto inferior esquerdo da janela
        update(&player);


    attron(COLOR_PAIR(COLOR_WHITE));
    for (i=0; i<lin; i++) {
        for (j=0; j<col; j++) {
            if ((i==0)||(j==0)||(i==lin-1)||(j==202)) mvaddch(i, j, '#' | A_BOLD);
        }
        j=0;
    }
    mvaddch(player.posx, player.posy, '@' | A_BOLD); //impressão do Jogador
    mvaddch(x_porta_nivel, y_porta_nivel, '>' | A_BOLD); //impressão da porta de nivel
    for (int k=0; k<199; k++)
        mvprintw(lin, k, " "); //limpar a linha de impressões para se poder voltar a escrever novamente
    refresh();
    mvprintw(lin, 95, "Pontuação: %d", score); //impressão da pontuação atual
    attroff(COLOR_PAIR(COLOR_WHITE));
    }

    return 0;
}
