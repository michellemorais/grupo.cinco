#include "monstros.c"
#include "mapa.c"

#define lin 50
#define col 203
#define linha 300
#define coluna 3

#define geradorLinha 42
#define geradorColuna 198


extern MONSTRO mob;
extern MONSTRO mobs[8];

extern JOGADOR player;
extern char matriz[lin][col];
extern int x_porta_nivel, y_porta_nivel, score, linha_impressões, qtdMonstros;
extern int matrizaux[linha][coluna], t, p, casa_y, casa_x;


void draw_Player (JOGADOR *player, int dx, int dy) {
    if (ehMonstro(player->posx+dx, player->posy+dy)){
        flash();
        score = 1;
        Gera_Nivel();
    }
    if (naoEhParedeOuMonstro(player->posx+dx, player->posy+dy)) { //caso a nova posição não seja um muro, move-se o jogador
        attron(COLOR_PAIR(COLOR_YELLOW));
        mvaddch(player->posx, player->posy, '.'); //coloca um espaço na posição antiga do jogador /////////////////////////////alterou-se
        attroff(COLOR_PAIR(COLOR_YELLOW));
        player->posx += dx;
        player->posy += dy;
        attron(COLOR_PAIR(COLOR_WHITE));
        mvaddch(player->posx, player->posy, '@' | A_BOLD); //coloca o jogador na sua nova posição
        attroff(COLOR_PAIR(COLOR_WHITE));

        moveMonstros();

        draw_Light(player); //desenhar luz em torno do jogador
    }

    if (ehPorta(player->posx, player->posy)) {score++; Gera_Nivel();} //caso a posição do jogador seja a mesma que a da porta de nivel, incrementa-se a pontuação e gera-se um novo nivel

}

void update(JOGADOR *player) {
    int key = getch();

    switch(key) {
        case KEY_A1:
        case '7': draw_Player(player, -1, -1); break;
        case KEY_UP:
        case '8': draw_Player(player, -1, +0); break;
        case KEY_A3:
        case '9': draw_Player(player, -1, +1); break;
        case KEY_LEFT:
        case '4': draw_Player(player, +0, -1); break;
        case KEY_B2:
        case '5': break;
        case KEY_RIGHT:
        case '6': draw_Player(player, +0, +1); break;
        case KEY_C1:
        case '1': draw_Player(player, +1, -1); break;
        case KEY_DOWN:
        case '2': draw_Player(player, +1, +0); break;
        case KEY_C3:
        case '3': draw_Player(player, +1, +1); break;
        case 'q': endwin(); exit(0); break; //usa-se o caracter 'Q' para sair do Jogo
    }
}
