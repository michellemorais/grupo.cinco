#include "verifica.c"

#define lin 50
#define col 203
#define linha 300
#define coluna 3

#define geradorLinha 42
#define geradorColuna 198


extern MONSTRO mob;
extern MONSTRO mobs[8];

extern JOGADOR player;
extern char matriz[lin][col];
extern int x_porta_nivel, y_porta_nivel, score, linha_impressões, qtdMonstros;
extern int matrizaux[linha][coluna], t, p, casa_y, casa_x;


int check(int px, int py) {
    int k=0, l=0;
    if (matrizaux[0][0]==0) return 0;
    for (; matrizaux[k][l]!=0; k++) { //verifica para todas as coordenadas salvaguardadas na matrizaux, se as coordenadas em questão (px e py) são de uma zona sem luz.
        if (matrizaux[k][l]==1) {
            if ((px<=matrizaux[k][l+1])&&(py<=matrizaux[k][l+2])) return 1; //para os diferentes casos, se as coordenadas estão numa zona sem luz retorna 1
        }
        if (matrizaux[k][l]==2) {
            if ((px<=matrizaux[k][l+1])&&(py>=matrizaux[k][l+2])) return 1;
        }
        if (matrizaux[k][l]==3) {
            if ((px>=matrizaux[k][l+1])&&(py>=matrizaux[k][l+2])) return 1;
        }
        if (matrizaux[k][l]==4) {
            if ((px>=matrizaux[k][l+1])&&(py<=matrizaux[k][l+2])) return 1;
        }
    }
    return 0; //retorna 0 caso contrário
}

void guarda_parede(JOGADOR *player){
    if ((casa_y<=(player->posy))&&(casa_x<(player->posx))) {matrizaux[t][p]=1; matrizaux[t][p+1]=casa_x; matrizaux[t][p+2]=casa_y; t++;} //1,2,3 e 4 são flags para indicar o sentido e direção de áreas sem luz
    if ((casa_y>(player->posy))&&(casa_x<=(player->posx))) {matrizaux[t][p]=2; matrizaux[t][p+1]=casa_x; matrizaux[t][p+2]=casa_y; t++;}
    if ((casa_y>=(player->posy))&&(casa_x>(player->posx))) {matrizaux[t][p]=3; matrizaux[t][p+1]=casa_x; matrizaux[t][p+2]=casa_y; t++;}
    if ((casa_y<(player->posy))&&(casa_x>=(player->posx))) {matrizaux[t][p]=4; matrizaux[t][p+1]=casa_x; matrizaux[t][p+2]=casa_y; t++;}
}

void draw_Light(JOGADOR *player) {

    int spin=-1, comp=2; // 'comp' corresponde ao comprimento do lado; 'spin' corresponde ao numero da iteração quadrangular, mas com valor negativo;

    for (t=0, p=0; t<200; t++) { //inicializar matrizaux a zeros; esta matriz permite armazenar flags(indicadoras de direção e sentido, utilizadas na função auxiliar check) e coordenadas de zonas do mapa sem luz
        matrizaux[t][p]=0;
        matrizaux[t][p+1]=0;
        matrizaux[t][p+2]=0;
    }
    t=p=0;
    attron(COLOR_PAIR(COLOR_YELLOW));
    do { //algoritmo dividido em 4 partes que correspondem aos lados de um quadrado, sucessivamente maior (por incremento do comp) ao fim de 1 ciclo, que avança uma a uma as posições fronteiriças do mesmo (tipo espiral).
        //Processo semelhante para cada um dos 4, sendo que no 1º inicia-se o ciclo atribuindo a (casa_x, casa_y), coordenadas em duvida para a colocação de luz, as coordenadas do jogador ambas com o decremento de uma unidade.
        for (casa_x=spin+player->posx, casa_y=spin+player->posy; casa_y < comp-1+player->posy; casa_y++) { //parte de cima do quadrado, sentido da esquerda para a direita
            if (casa_x<=0||casa_x>=lin-1||casa_y<=0||casa_y>=col-1) continue; //evitar sair do mapa
            if (check(casa_x, casa_y)) { //se a posição (casa_x,casa_y) encontra-se numa zona sem luz
                if (naoEhParede(casa_x, casa_y) && naoEhPortaOuMonstro(casa_x, casa_y)) mvaddch(casa_x, casa_y, ' '); //se essa posição não for uma parde ou a porta de nivel fica escondida (coloca-se um espaço)
                continue;
            }
            if (ehParede(casa_x, casa_y)) { //se as coordenadas em questão são relativas a uma parede, não há luz em determinadas direções (definem-se 4 quadrantes possiveis)
                guarda_parede(player);} else {if (naoEhPortaOuMonstro(casa_x, casa_y)) mvaddch(casa_x, casa_y, '.');} //caso as coordenadas não sejam as de uma parede, nem da porta de nível, e como a posição não corresponde a uma zona sem luz (check (casa_x,casa_y) retornou o valor 0), as coordenadas em questão passam a ter luz '.'
        }

        for (; casa_x < comp-1+player->posx; casa_x++) { //parte direita do quadrado, sentido de cima para baixo
            if (casa_x<=0||casa_x>=lin-1||casa_y<=0||casa_y>=col-1) continue;
            if (check(casa_x, casa_y)) {
                if (naoEhParede(casa_x, casa_y) && naoEhPortaOuMonstro(casa_x, casa_y)) mvaddch(casa_x, casa_y, ' ');
                continue;
            }
            if (ehParede(casa_x, casa_y)) {guarda_parede(player);} else {if (naoEhPortaOuMonstro(casa_x, casa_y)) mvaddch(casa_x, casa_y, '.');}
        }

        for (; casa_y > (player->posy)-comp+1; casa_y--) { //parte de baixo do quadrado, sentido da direita para a esquerda
            if (casa_x<=0||casa_x>=lin-1||casa_y<=0||casa_y>=col-1) continue;
            if (check(casa_x, casa_y)) {
                if (naoEhParede(casa_x, casa_y) && naoEhPortaOuMonstro(casa_x, casa_y)) mvaddch(casa_x, casa_y, ' ');
                continue;
            }
            if (ehParede(casa_x, casa_y)) {guarda_parede(player);} else {if (naoEhPortaOuMonstro(casa_x, casa_y)) mvaddch(casa_x, casa_y, '.');}
        }

        for (; casa_x > (player->posx)-comp+1; casa_x--) { //parte esquerda do quadrado, sentido de baixo para cima
            if (casa_x<=0||casa_x>=lin-1||casa_y<=0||casa_y>=col-1) continue;
            if (check(casa_x, casa_y)) {
                if ( naoEhParede(casa_x, casa_y) && naoEhPortaOuMonstro(casa_x, casa_y)) mvaddch(casa_x, casa_y, ' ');
                continue;
            }
            if (ehParede(casa_x, casa_y)) {guarda_parede(player);} else {if (naoEhPortaOuMonstro(casa_x, casa_y)) mvaddch(casa_x, casa_y, '.');}
        }

        spin = spin-1; //descer uma casa, para a proxima iteração
        comp++; //aumentar comprimento do lado do quadrado

        if (comp==col) break; //col é a distancia máxima do jogador á posição mais distante do mapa, e portanto comprimento maximo do ciclo

    } while(1);
    attroff(COLOR_PAIR(COLOR_YELLOW));

    attron(COLOR_PAIR(COLOR_MAGENTA)); //esconder todas as restantes posições do mapa, menos a porta de nivel
    for (t=1; t<lin-1; t++)
        for (p=1; p<col-1; p++)
            if (check(t,p) && naoEhPortaOuMonstro(t, p)) {
                if (matriz[t][p]=='#') mvaddch(t, p, '#' | A_BOLD);
                else mvaddch(t, p, ' ');
            }
    attroff(COLOR_PAIR(COLOR_MAGENTA));

    for (t=0, p=0; t<200; t++) { //repor matrizaux a zeros
        matrizaux[t][p]=0;
        matrizaux[t][p+1]=0;
        matrizaux[t][p+2]=0;
    }
}
