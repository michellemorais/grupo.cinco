
/* Inicializa o gerador de nºs aleatórios com um nº que depende da hora
 a que se inicializa a função */
void Gera_seed (void) {
    long seed;
    time(&seed);
    srand( (unsigned) seed);
}

void Gera_Nivel (void) { //gera o mapa e a posição inicial do jogador
    int i, j, linha_aleatoria, coluna_aleatoria, linha_vazia, num_quadrados, lado_quadrado, lado1_quadrado, lado2_quadrado, guarda_coluna;
    int posx_inicial, posy_inicial;

    Gera_seed();

    for (i=0; i<lin; i++) {
        for (j=0; j<col; j++) {
            if (i==0||i==lin-1||j==0||j==col-1) matriz[i][j]='#'; //fazer os bordos
            else matriz[i][j]=' ';
        }
        j=0;
    }

    num_quadrados = 20 + rand()%51; //maximo de 70 quadrados

    linha_vazia = 1 + rand()%geradorLinha;
    posx_inicial=linha_vazia;

    do {
        lado_quadrado = 1 + rand()%20; // maximo de 20 para o valor do lado de um quadrado
        lado1_quadrado = lado2_quadrado  = lado_quadrado;

        //gerar aleatóriamente as coordenadas do quadrado (assumindo que correspondem ao vertice superior esquerdo) na matriz
        linha_aleatoria = 1 + rand()%geradorLinha; //rand()%geradorLinha gera aleatoriamente um numero entre 0 e 57, ao qual adicionamos 1 para que não ocorrer a primeira linha nem a última, que correspondem a bordos
        coluna_aleatoria = 1 + rand()%geradorColuna; // a mesma coisa repete-se para as colunas
        guarda_coluna=coluna_aleatoria;

        //desenha o respetivo quadrado na matriz
        for ( ; lado1_quadrado > 0 && linha_aleatoria < geradorLinha + 1; linha_aleatoria++, lado1_quadrado--) {
            for ( ; lado2_quadrado > 0 && coluna_aleatoria < geradorColuna + 1; coluna_aleatoria++, lado2_quadrado--) {
                if (linha_aleatoria==linha_vazia) continue; //criação de uma linha livre, onde surge o jogador
                else matriz[linha_aleatoria][coluna_aleatoria]='#';
            }
            coluna_aleatoria=guarda_coluna; //para repor indice da coluna e tamanho do lado
            lado2_quadrado=lado_quadrado;
        }

        num_quadrados--;

    } while (num_quadrados>0);

    do {
        posy_inicial = 1 + rand()%geradorColuna;
        if (matriz[posx_inicial][posy_inicial]!='#') break; //verifica se a posição está livre
    } while (1);

    player.posx = posx_inicial;
    player.posy = posy_inicial;

    do { //criação da porta de nivel que permite subir de nivel
        x_porta_nivel = 1 + rand()%geradorLinha;
        y_porta_nivel = 1 + rand()%geradorColuna;
        if (naoEhParede(x_porta_nivel, y_porta_nivel) && naoEhPorta(posx_inicial, posy_inicial)) break; //verifica se a posição está livre (se não é a posição de uma parede ou da posição inicial do jogador)
    } while (1);

    for (j=1; j+1<col; j++)
        matriz[x_porta_nivel][j]=' '; //criação de uma linha livre, onde surge a porta de nivel

    //Impressão do Mapa na Janela
    attron(COLOR_PAIR(COLOR_WHITE));
    for (i=0; i<lin; i++) {
        for (j=0; j<col; j++) {
            if (matriz[i][j]=='#') mvaddch(i, j, '#' | A_BOLD);
            else mvaddch(i, j, ' ');
        }
        j=0;
    }
    mvaddch(player.posx, player.posy, '@' | A_BOLD); //impressão do Jogador
    mvaddch(x_porta_nivel, y_porta_nivel, '>' | A_BOLD); //impressão da porta de nivel
    attroff(COLOR_PAIR(COLOR_WHITE));

    attron(COLOR_PAIR(COLOR_CYAN));
    qtdMonstros = score > 8 ? 8 : score/2;
    geraMonstros(qtdMonstros);
    attroff(COLOR_PAIR(COLOR_CYAN));

    
    attron(COLOR_PAIR(COLOR_WHITE));
    for (j=0; j<199; j++)
        mvprintw(linha_impressões, j, " "); //limpar a linha de impressões para se poder voltar a escrever novamente
    refresh();
    mvprintw(lin, 95, "Pontuação: %d", score); //impressão da pontuação atual
    attroff(COLOR_PAIR(COLOR_WHITE)); 
    draw_Light(&player);
}
